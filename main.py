from paho.mqtt import client as mqtt_client


broker = 'mqtt.fllscoring.nl'
port = 1883
topic = "fll/button/request"
# generate client ID with pub prefix randomly
client_id = f'button-assigner'

assignments = {
    "BUTTON_964DB8": "Development",
    "BUTTON_F95B6C": "Eindhoven",
    "button3": "Zeeland"
}


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
#    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        try:
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` `{assignments[msg.payload.decode()]}`")
            client.publish(f"fll/button/{msg.payload.decode()}/assign", assignments[msg.payload.decode()])
        except KeyError:
            print(f"No things found for `{msg.payload.decode()}` from `{msg.topic}`")
            client.publish(f"fll/button/{msg.payload.decode()}/assign", "localhost")

    client.subscribe(topic)
    client.on_message = on_message


def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


if __name__ == '__main__':
    run()
